import React, { useEffect, useState } from 'react';
import './index.css';

const zolozUrl = 'https://sg-production-cdn.zoloz.com/page/zoloz-face-fe/index.html?state=G000000004FFC20230525000000051190811338&clientcfg=eyJzaWduIjoiWlYyQkFLT0tYdXJPU3dJaTlYWVRFdEtlTHNqMmE4eDNxZllEWFF5VTF4aERZWVEvTzRMcE9rN2FHRVpmU1k1eHpnR1RaRDhKZFpEOFhMQ1lqTC95K0VhazFRZFFITEp3dGxuWFJVOU81bjlTY0J1cjhDQVQva2JxdjA3akM2NVdzRzlGRXkvVGpIMkpKVWd2V0JpRW5tMEJsOVlKRGdtZVJVMXJFMmZ2SWxhVFVmY0lFaDlHYjQ5MmZuMjQvbXVQelVQMEtaMDd3ek1PZHYxQTYxaC9lWStCOVlDYzB1d0pob0lDNU9RM2RqYTNwZ3R2ZFk2VjIyamJibUlkZ3d0UUZIV0pzUk4vZXllVjk2OWlBTnJYQTZaWklwZ1llTmVjL2pmakFOSElHZ2JnTEpiMlpHdjNPNkJkR2pOWTBEZHdYZEdveXhkNXpsMW9DUHI1SWN3bmpnPT0iLCJjb250ZW50Ijoie1wiR0FURVdBWV9VUkxcIjpcImh0dHBzOi8vc2ctc2FuZGJveC1hcGkuem9sb3ouY29tL3ptZ3MvdjIvc2VjXCIsXCJBUFBfSURcIjpcIlwiLFwia2V5TWV0YVwiOlwie1xcXCJtaWRcXFwiOlxcXCIxMTM1XFxcIixcXFwia2V5VmVyXFxcIjpcXFwidjFcXFwifVwiLFwiaW50ZXJydXB0Q2FsbGJhY2tVcmxcIjpcImh0dHBzOi8vZGV2LmJpb25zLmlkL3pvbG96L2Vycm9yXCIsXCJXT1JLU1BBQ0VfSURcIjpcIlwiLFwiemltSWRcIjpcIkcwMDAwMDAwMDQyMzMzM2YzOTMxOTAyNjQ2MzAzZWYzYTcwYWRkOTQzYVwiLFwic3RhdGVcIjpcIkcwMDAwMDAwMDRGRkMyMDIzMDUyNTAwMDAwMDA1MTE5MDgxMTMzOFwiLFwiY29tcGxldGVDYWxsYmFja1VybFwiOlwiaHR0cHM6Ly9kZXYuYmlvbnMuaWQvem9sb3ovY29tcGxldGVcIixcIkMyU19QVUJfS0VZXCI6XCJNSUlCSWpBTkJna3Foa2lHOXcwQkFRRUZBQU9DQVE4QU1JSUJDZ0tDQVFFQXU3NWJtdzM5YWhUckIwS21KK2h2STlVUU4yc2JoV0xnTC9KSEdRazlxNlh6SVZQWVU0R1hrT3IwbXlXc1B6YmdFZkZiNDFyMFlpNGFIRlhYbDNtcXRkb2ROTXN3ME1tem5JcmpMU3NDbmp2MWtwQ1dRWnptZi9hSVoxY3lsUWlKS2lWNmU5VEtHQ3RiZjJzeElWVXNxZjRoK2hqU0s3eDZ4ejV5UUVQeTkrTStxOHVkWEY4cTRxaVVuR3dYWSs0VW9HYVh3YkdkaWR6aVVtU2I4YTJXRG44UDU5N1Z1T2t2b29YZkQyREJ5SmNMMkRQRUJFVS9uL2lGYzVpbnl6RDl1SzA4NjNUeEt3RXF4VmRHZHNaZElhNUJZa2pQTUl0OW1kQm9ZN250UzRrV0lpSGNRVWlQR2xGejMxK0JtaHdhSitJYmh5MG9vSmJwRnZPQ2d0U29yUUlEQVFBQlwiLFwiUkVNT1RFTE9HX1VSTFwiOlwiaHR0cHM6Ly9zZy1zYW5kYm94LWFwaS56b2xvei5jb20vbG9nZ3cvbG9nVXBsb2FkLmRvXCJ9In0=&callbackurl=https://dev.bions.id/zoloz/complete&langPack=https://dev.bions.id:8000/conf/config/json';

const App = () => {
  const [cameraPermission, setCameraPermission] = useState(false);

  const handleIframeLoad = () => {
    const iframe = document.getElementById('ekyc-iframe') as HTMLIFrameElement;
    if (iframe) {
      const currentURL = iframe.contentWindow?.location.href;
      console.log('[DEBUG] currentURL : ', currentURL);
    }
  }

  useEffect(() => {
    if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
      navigator.mediaDevices.getUserMedia({ video: true })
        .then(() => {
          setCameraPermission(true);
        })
        .catch((error) => {
          alert('Camera Permission Error : ' + JSON.stringify(error));
          setCameraPermission(false);
        });
    }
  }, [])

  return cameraPermission ? (
    <iframe
      id='ekyc-iframe'
      src={zolozUrl}
      style={{ height: '100vh', width: '100vw' }}
      onLoad={handleIframeLoad}
    />
  ) : (
    <p>Loading camera access ...</p>
  )
}

export default App;